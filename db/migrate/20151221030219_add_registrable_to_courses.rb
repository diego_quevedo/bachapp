class AddRegistrableToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :registrable, :boolean
  end
end
