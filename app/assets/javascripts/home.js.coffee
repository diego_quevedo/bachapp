# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ($) (e) ->

  $navbar = $("#nav")
  $home = $("#home")

  $(window).scroll ->
    if $(window).scrollTop() > $home.height()/2
      $navbar.addClass('affix')
    else
      $navbar.removeClass('affix')

  # auto scrolls for you to the next section
  $('#continue-button').click ->
    offset = 1
    $('html, body').animate { scrollTop: $('#about').offset().top + offset}, 1000
    return false

  $('.dropdown-toggle').dropdown()
