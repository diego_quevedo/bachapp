class ContactRequest < ActiveRecord::Base
  validates :name, :email, :message, presence: true
  validates :name, :email, length: { maximum: 50 }

  after_create :send_contact_email

  def send_contact_email
    ContactRequestMailer.contact_email(self).deliver
  end
end
