class Course < ActiveRecord::Base
  belongs_to :user
  has_attached_file :document, styles: {thumb: ["100x100", :jpg]}
  validates_attachment_content_type :document, :content_type => /\Aimage\/.*\Z|\Aapplication\/pdf\Z/
end
