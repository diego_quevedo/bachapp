class ContactRequestMailer < ActionMailer::Base
  layout 'email'
  default from: "contacto@floresdebachmexico.com"

  def contact_email(contact_request)
    @contact_request = contact_request
    recipients = %w(alma@floresdebachmexico.com alejandro@floresdebachmexico.com)
    mail(to: recipients, subject: "Solicitud de contacto de #{contact_request.name}")
  end
end
