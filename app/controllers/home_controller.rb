class HomeController < ApplicationController
  def index
    @contact_request = ContactRequest.new
    @entries = Entry.all
    @courses = Course.all
  end
end
