class ContactRequestsController < ApplicationController
  def create
    @contact_request = ContactRequest.new(contact_request_params)

    redirect_to root_url
    if @contact_request.save
      flash[:notice] = "se ha enviado correctamente"
    else
      flash[:alert] = "No se pudo enviar. Intente nuevamente"
    end
  end

  private

  def contact_request_params
    params.require(:contact_request).permit(:name, :email, :message)
  end
end
