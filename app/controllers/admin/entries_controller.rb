class Admin::EntriesController < ApplicationController
  before_action :check_admin_permissions
  before_action :find_entry, only: [:edit, :update, :destroy]

  def index
    @entries = Entry.all
  end

  def new
    @entry = Entry.new
  end

  def create
    @entry = Entry.new(entry_params)
    if @entry.save
      flash[:notice] = "El curso se creó exitosamente"
      redirect_to admin_entries_path
    else
      render 'new'
    end
  end

  def edit

  end

  def update
    if @entry.update(entry_params)
      flash[:notice] = "Curso editado"
      redirect_to admin_entries_path
    else
      render 'edit'
    end
  end

  def destroy
    @entry.destroy
    redirect_to admin_entries_path
  end

  private

    def find_entry
      @entry = Entry.find(params[:id])
    end

    def entry_params
      params.require(:entry).permit(:url).merge(user_id: current_user.id)
    end
end
