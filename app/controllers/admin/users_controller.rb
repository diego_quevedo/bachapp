class Admin::UsersController < ApplicationController
  before_action :check_admin_permissions
  before_action :find_user, only: [:edit, :update]

  def index
    @users = User.all
    # if params[:query]
    #   @query = params[:query]
    #   @users = @users.search(params[:query])
    # end
  end

  def edit
  end

  def update
    if @user.update_attributes(user_params)
      flash[:notice] = "Perfil editado"
      redirect_to admin_users_path
    else
      render 'edit'
    end
  end


  protected

    def find_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:name, :email, :admin)
    end

end
