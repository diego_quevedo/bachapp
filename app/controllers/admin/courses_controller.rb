class Admin::CoursesController < ApplicationController
  before_action :check_admin_permissions
  before_action :find_course, only: [:edit, :update, :destroy]

  def index
    @courses = Course.all
  end

  def new
    @course = Course.new
  end

  def create
    @course = Course.new(course_params)
    if @course.save
      flash[:notice] = "El curso se creó exitosamente"
      redirect_to admin_courses_path
    else
      render 'new'
    end
  end

  def edit

  end

  def update
    if @course.update(course_params)
      flash[:notice] = "Curso editado"
      redirect_to admin_courses_path
    else
      render 'edit'
    end
  end

  def destroy
    @course.destroy
    redirect_to admin_courses_path
  end

  private

    def find_course
      @course = Course.find(params[:id])
    end

    def course_params
      params.require(:course).permit(:title, :description, :cost, :document, :registrable)
    end

end
