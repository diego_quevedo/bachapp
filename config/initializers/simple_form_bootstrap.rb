# Use this setup block to configure all options available in SimpleForm.
SimpleForm.setup do |config|
  config.wrappers :text_input, tag: "div", class: "form-group", error_class: :field_with_errors do |b|
    b.use :html5
    b.use :placeholder

    # Calculates maxlength from length validations for string inputs
    b.use :maxlength

    # Calculates pattern from format validations for string inputs
    b.use :pattern

    # Calculates min and max from length validations for numeric inputs
    b.use :min_max

    # Calculates readonly automatically from readonly attributes
    b.use :readonly

    b.use :label, wrap_with: { tag: "h6" }
    b.use :hint,  wrap_with: { tag: "p", class: "help-block" }
    b.wrapper :controls, tag: "div", class: "controls" do |input|
      input.use :input
      input.use :error, wrap_with: { tag: "span", class: "label label-danger pull-right" }
    end
  end

  config.wrappers :select_input, tag: "div", class: "form-group", error_class: :field_with_errors do |b|
    b.use :html5
    b.use :placeholder
    b.use :maxlength
    b.use :pattern
    b.use :min_max
    b.use :readonly

    b.use :label, wrap_with: { tag: "h4" }
    b.use :hint,  wrap_with: { tag: "p", class: "help-block" }
    b.wrapper :controls, tag: "div", class: "controls" do |ba|
      ba.use :input, wrap_with: { tag: "div", class: "form-control"}
      ba.use :error, wrap_with: { tag: "span", class: "label label-danger" }
    end
  end

  config.wrappers :addon_input, tag: "div", class: "form-group", error_class: :field_with_errors do |b|
    b.use :html5
    b.use :placeholder
    b.use :maxlength
    b.use :pattern
    b.use :min_max
    b.use :readonly

    b.use :judge
    b.use :label, wrap_with: { tag: "h4" }
    b.use :hint,  wrap_with: { tag: "p", class: "help-block" }
    b.wrapper :controls, tag: "div", class: "controls input-group" do |ba|
      ba.use :input
      ba.use :error, wrap_with: { tag: "span", class: "label label-danger" }
    end
  end

  # config.wrappers :inline_checkbox, :tag => 'div', :class => 'control-group', :error_class => 'error' do |b|
  #   b.use :html5
  #   b.wrapper :tag => 'div', :class => 'controls' do |ba|
  #     ba.wrapper :tag => 'label', :class => 'checkbox' do |bb|
  #       bb.use :input
  #       bb.use :label_text
  #     end
  #     ba.use :error, :wrap_with => { :tag => 'span', :class => 'help-inline' }
  #     ba.use :hint,  :wrap_with => { :tag => 'p', :class => 'help-block' }
  #   end
  # end

  config.wrappers :checkbox, tag: :div, class: "checkbox", error_class: "has-error" do |b|
      # Form extensions
      b.use :html5
      # Form components
      b.wrapper tag: :label do |ba|
        ba.use :input
        ba.use :label_text
      end
      b.use :hint,  wrap_with: { tag: :p, class: "help-block" }
      b.use :error, wrap_with: { tag: :span, class: "help-block text-danger" }
    end
  # Wrappers for forms and inputs using the Twitter Bootstrap toolkit.
  # Check the Bootstrap docs (http://twitter.github.com/bootstrap)
  # to learn about the different styles for forms and inputs,
  # buttons and other elements.
  config.default_wrapper = :text_input
  config.input_class = "form-control"
  config.required_by_default = false
end
